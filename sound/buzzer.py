import time
import threading
from ..system.io.gpio import *
from ..utils.logger import log

FREQS = [
        [65.406,69.296,73.416,77.782,82.407,87.307,92.499,97.999,103.826,110,116.541,123.471],
        [130.813,138.591,146.832,155.563,164.814,174.614,184.997,195.998,207.652,220,233.082,246.942],
        [261.626,277.183,293.665,311.127,329.628,349.228,369.994,391.995,415.305,440,466.164,493.883],
        [523.251,554.365,587.33,622.254,659.255,698.456,739.989,783.991,830.609,880,932.328,987.767],
        [1046.502,1108.731,1174.659,1244.508,1318.51,1396.913,1479.978,1567.982,1661.219,1760,1864.655,1975.533],
        [2093.005,2217.461,2349.318,2489.016,2637.02,2793.826,2959.955,3135.963,3322.438,3520,3729.31,3951.066],
        [4186.009,4434.922,4698.636,4978.032,5274.041,5587.652,5919.911,6271.927,6644.875,7040,7458.62,7902.133],
        [8372.018,8869.844,9397.273,9956.063,10548.082,11175.303,11839.822,12543.854,13289.75,14080,14917.24,15804.266]
]

STYPES = {'BAD'   : 369.994,
          'OK'    : 1000,
          'ALERT' : 2489.016}

class Buzzer:
    MODE = 0 # 0 - Discontinuous // 1 - Continuous
    PINS = [27]
    STATUS = []

    def __init__(self, **kwargs):
        if "pins" in kwargs:
            self.PINS = list(map(int, kwargs['pins'].split(",")))
        if "mode" in kwargs:
            self.MODE = int(kwargs['mode'])

        log.debug("Initializing Buzzer: (Mode: %d, Pins: %s)" % (self.MODE,','.join(str(x) for x in self.PINS)))
        if self.MODE == 1: log.debug("CONTINUOUS: All the sounds will be equal")
        self.GPIO = []
        for pos,pin in enumerate(self.PINS):
            g = GPIO(pin)
            g.setup(OUT)
            self.GPIO.append(g)
            self.STATUS.append(False)
            self.deactivate(pos)

    def isActive(self, bnum):
        if bnum < len(self.PINS):
            return self.STATUS[bnum]
        return False

    def activate (self, bnum):
        if bnum < len(self.PINS):
            self.STATUS[bnum] = True
            self.GPIO[bnum].write(HIGH)

    def deactivate(self, bnum):
        if bnum < len(self.PINS):
            self.STATUS[bnum] = False
            self.GPIO[bnum].write(LOW)

    def activateDuring(self, bnum, secs, status="OK"):
        thread = threading.Thread(target=self.playTimed,args=(bnum, secs, status,))
        thread.start()

    def microDelay(self, n):
        time.sleep(n/1000000.0)


    def playTimed(self, bnum, secs, status):
        log.info("Starting beep in buzzer: %s" % self.PINS[bnum])
        if self.MODE == 0: # Discontinous
            freq=STYPES[status]
            log.debug("FREQUENCY: %s (%s)" % (str(freq), status))
            #t=1/freq
            tone = 1000000/freq
            loop = (secs*800000)/(tone*2)
            t=time.time()
            for i in range(0,int(loop)):
                self.GPIO[bnum].write(HIGH)
                time.sleep(tone/1000000)
                self.GPIO[bnum].write(LOW)
                time.sleep(tone/1000000)
        elif self.MODE == 1: # Continuos
            self.activate(bnum)
            time.sleep(secs)
            self.deactivate(bnum)
        log.info("Ending beep in buzzer: %s" % self.PINS[bnum])
