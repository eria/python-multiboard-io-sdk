from ..system.io.pwm import *
import time

class Servo:
    MIN_DUTY = 3
    MAX_DUTY = 14.5
    
    def __init__(self, pin, pos=0):
        self.PWM = PWM(pin)
        self.PWM.start((pos / 180) * (self.MAX_DUTY-self.MIN_DUTY) + self.MIN_DUTY, 60)
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.stop()
        
    def stop(self):
        self.PWM.stop() 
            
    def write(self, pos):
        self.PWM.setDutyCycle((pos / 180) * (self.MAX_DUTY-self.MIN_DUTY) + self.MIN_DUTY)
