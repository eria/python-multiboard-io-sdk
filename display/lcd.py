import time
from ..system.io.gpio import *
from ..utils.logger import log

class LCD:
    WIDTH = 16
    LINES = 2
    PINS  = [8, 24, 23, 18, 15, 14] # [RS,E,D4,D5,D6,D7]
    VCC_CONTROL = 0
    VCC_CONTROL_PIN = 16   

    LCD_CHR = HIGH
    LCD_CMD = LOW

    LINE_DIRS = [ 0x80, 0xC0, 0x00, 0x00 ]

    # Timing constants
    E_PULSE = 0.00010
    E_DELAY = 0.00010
    
    BLOCKED=False
    
    def __init__(self, **kwargs):
        if "pins" in kwargs:
            l = list(map(int, kwargs['pins'].split(",")))
            if len(l) == 6: self.PINS = l
        if "width" in kwargs:
            self.WIDTH = int(kwargs['width'])            
        if "lines" in kwargs:
            self.LINES = int(kwargs['lines'])
        if "vcc_control" in kwargs:
            self.VCC_CONTROL = int(kwargs['vcc_control'])
        if "vcc_control_pin" in kwargs:
            self.VCC_CONTROL_PIN = int(kwargs['vcc_control_pin'])

        if self.VCC_CONTROL:
            self.connect_supply()
        
        log.debug("Initializing LCD: (Pins: %s, Lines: %d, Width: %d)" % (
            ','.join(str(x) for x in self.PINS), self.LINES, self.WIDTH))
        self.GPIO = []
        for i in self.PINS:
            g = GPIO(i)
            g.setup(OUT)
            self.GPIO.append(g)

    def connect_supply(self):
        if not self.VCC_CONTROL:
            log.debug("Supply is allways ON, cannot be switched")
            return True
        try:
            GPIO_VCC = GPIO(self.VCC_CONTROL_PIN)
            GPIO_VCC.setup(OUT)
            if GPIO_VCC:
                log.info("Connecting LCD supply")
                GPIO_VCC.write(HIGH)
            else:
                log.error("Supply control cannot be controlled")
        except:
            log.error("LCD cannot be plugged")
            raise

    def disconnect_supply(self):
        if not self.VCC_CONTROL:
            log.debug("Supply is allways ON, cannot be switched")
            return True
        try:
            GPIO_VCC = GPIO(self.VCC_PIN_CONTROL)
            GPIO_VCC.setup(OUT)
            if GPIO_VCC:
                log.info("Disconnecting LCD supply")
                GPIO_VCC.write(LOW)
            else:
                log.error("Supply control cannot be controlled")
        except:
            log.error("LCD cannot be unplugged")
            raise
    
    def show(self, *args):
        while self.BLOCKED: continue
        self.BLOCKED=True
        
        nline=0
        self.clear()
        for text in args:
            self.lcd_byte(self.LINE_DIRS[nline], self.LCD_CMD)
            ftext = text.ljust(self.WIDTH, " ")
            for i in range(self.WIDTH):
                self.lcd_byte(ord(ftext[i]),self.LCD_CHR)
            nline+=1
            if nline >= self.LINES: break
        
        self.BLOCKED=False

    def clear(self):
        # Initialise display
        self.lcd_byte(0x33,self.LCD_CMD)
        self.lcd_byte(0x32,self.LCD_CMD)
        self.lcd_byte(0x28,self.LCD_CMD)
        self.lcd_byte(0x0C,self.LCD_CMD)  
        self.lcd_byte(0x06,self.LCD_CMD)
        self.lcd_byte(0x01,self.LCD_CMD) 
        time.sleep(0.001)


    def lcd_byte(self, bits, mode):
        # Send byte to data pins
        # bits = data
        # mode = True  for character
        #        False for command

        self.GPIO[0].write(mode) # RS

        # High bits
        self.GPIO[2].write(LOW)
        self.GPIO[3].write(LOW)
        self.GPIO[4].write(LOW)
        self.GPIO[5].write(LOW)
        if bits&0x10==0x10:
            self.GPIO[2].write(HIGH)
        if bits&0x20==0x20:
            self.GPIO[3].write(HIGH)
        if bits&0x40==0x40:
            self.GPIO[4].write(HIGH)
        if bits&0x80==0x80:
            self.GPIO[5].write(HIGH)

        # Toggle 'Enable' pin
        time.sleep(self.E_DELAY)    
        self.GPIO[1].write(HIGH)  
        time.sleep(self.E_PULSE)
        self.GPIO[1].write(LOW)  
        time.sleep(self.E_DELAY)      

        # Low bits
        self.GPIO[2].write(LOW)
        self.GPIO[3].write(LOW)
        self.GPIO[4].write(LOW)
        self.GPIO[5].write(LOW)
        if bits&0x01==0x01:
            self.GPIO[2].write(HIGH)
        if bits&0x02==0x02:
            self.GPIO[3].write(HIGH)
        if bits&0x04==0x04:
            self.GPIO[4].write(HIGH)
        if bits&0x08==0x08:
            self.GPIO[5].write(HIGH)

        # Toggle 'Enable' pin
        time.sleep(self.E_DELAY)    
        self.GPIO[1].write(HIGH)  
        time.sleep(self.E_PULSE)
        self.GPIO[1].write(LOW)  
        time.sleep(self.E_DELAY)

    def clear(self):
        # Initialise display
        self.lcd_byte(0x33,self.LCD_CMD)
        self.lcd_byte(0x32,self.LCD_CMD)
        self.lcd_byte(0x28,self.LCD_CMD)
        self.lcd_byte(0x0C,self.LCD_CMD)  
        self.lcd_byte(0x06,self.LCD_CMD)
        self.lcd_byte(0x01,self.LCD_CMD)    
    
        
        
        
