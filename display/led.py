import time
import threading
from ..system.io.gpio import *
from ..utils.logger import log

class Led:
    PINS = [4]
    STATUS = []

    def __init__(self, **kwargs):
        if "pins" in kwargs:
            self.PINS = list(map(int, kwargs['pins'].split(",")))

        log.debug("Initializing Led: (Pins: %s)" % (','.join(str(x) for x in self.PINS)))
        self.GPIO = []
        for pos,pin in enumerate(self.PINS):
            g = GPIO(pin)
            g.setup(OUT)
            self.GPIO.append(g)
            self.STATUS.append(False)
            self.deactivate(pos)

    def isActive(self, lnum):
        if lnum < len(self.PINS):
            return self.STATUS[lnum]
        return False

    def activate (self, lnum):
        if lnum < len(self.PINS):
            self.STATUS[lnum] = True
            self.GPIO[lnum].write(HIGH)

    def deactivate(self, lnum):
        if lnum < len(self.PINS):
            self.STATUS[lnum] = False
            self.GPIO[lnum].write(LOW)

    def activateDuring(self, lnum, secs):
        thread = threading.Thread(target=self.timed,args=(lnum, secs))
        thread.start()

    def timed(self, lnum, secs):
        self.activate(lnum)
        time.sleep(secs)
        self.deactivate(lnum)

