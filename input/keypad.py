from ..system.io.gpio import *
from ..utils.logger import log
import time

# Remember u need a good ground system ;P
class Keypad:
    KEYPAD = [
        [ 1 , 2 , 3 ,"a"],
        [ 4 , 5 , 6 ,"b"],
        [ 7 , 8 , 9 ,"c"],
        ["*", 0 ,"#","d"]
    ]

    ROW_PINS = [10,9,11,7]
    COL_PINS = [22,25,17,4]
    
    def __init__(self, **kwargs):
        if "row_pins" in kwargs:
            self.ROW_PINS = list(map(int, kwargs['row_pins'].split(",")))
        if "col_pins" in kwargs:
            self.COL_PINS = list(map(int, kwargs['col_pins'].split(",")))
        
        log.debug("Initializing Keypad: (Rows: %s, Cols: %s)" % (
            ','.join(str(x) for x in self.ROW_PINS), ','.join(str(x) for x in self.COL_PINS)))
        self.ROW_GPIO = []
        for i in self.ROW_PINS:
            self.ROW_GPIO.append(GPIO(i))
        
        self.COL_GPIO = []
        for i in self.COL_PINS:
            self.COL_GPIO.append(GPIO(i))
            
    
    def getKey(self):
        # Set all columns as output low
        for gpio in self.COL_GPIO:
            gpio.setup(OUT)
            gpio.write(HIGH)
        
        # Set all rows as input
        for gpio in self.ROW_GPIO:
            gpio.setup(IN)
            
        time.sleep(0.1)
        
        # Scan rows for pushed key/button
        # A valid key press should set "rowVal"  between 0 and 3.
        rowVal = -1
        for i in range(len(self.ROW_GPIO)):
            tmpRead = self.ROW_GPIO[i].read()
            if tmpRead == 1: 
                rowVal = i
                break
        
        # if rowVal is not 0 thru 3 then no button was pressed and we can exit
        if rowVal < 0 or rowVal >= len(self.ROW_GPIO):
            return
        
        # Convert columns to input
        for gpio in self.COL_GPIO:
            gpio.setup(IN)

        # Switch the i-th row found from scan to output
        self.ROW_GPIO[rowVal].setup(OUT)
        self.ROW_GPIO[rowVal].write(LOW)
        
#        time.sleep(0.1)

        # Scan columns for still-pushed key/button
        # A valid key press should set "colVal"  between 0 and 2.
        colVal = -1
        for j in range(len(self.COL_GPIO)):
            tmpRead = self.COL_GPIO[j].read()
            if tmpRead == 0: 
                colVal=j
                break
                
        # if colVal is not 0 thru 2 then no button was pressed and we can exit
        if colVal < 0 or colVal >= len(self.COL_GPIO):
            return

        # Return the value of the key pressed
        return self.KEYPAD[colVal][rowVal]
        
        


