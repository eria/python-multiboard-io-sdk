import RPi.GPIO as GPIO
from ..utils.logger import log

GPIO.setwarnings(False)

class Keypad:
    # CONSTANTS   
    KEYPAD = [
        [ 1 , 2 , 3 ,"a"],
        [ 4 , 5 , 6 ,"b"],
        [ 7 , 8 , 9 ,"c"],
        ["*", 0 ,"#","d"]

    ]

    # COLUMN = [9,10,22,17]
    # ROW    = [12,6,5,11]
    
    COLUMN   = [10,9,11,7]
    ROW      = [22,25,17,4]
    PULL_DOWN = 0 # 0 - Software pull_down, 1 - Hardware pull_down

    def __init__(self, **kwargs):
        GPIO.setmode(GPIO.BCM)
        if "pull_down" in kwargs:
            self.PULL_DOWN = int(kwargs['pull_down'])
        if "row_pins" in kwargs:
            self.ROW = list(map(int, kwargs['row_pins'].split(",")))
        if "col_pins" in kwargs:
            self.COLUMN = list(map(int, kwargs['col_pins'].split(",")))
        log.debug("Initializing Keypad: (Rows: %s, Cols: %s, Pull-Down: %d)" % (
            ','.join(str(x) for x in self.ROW), ','.join(str(x) for x in self.COLUMN), self.PULL_DOWN))

    def getKey(self):

        # Set all columns as output low
        for j in range(len(self.COLUMN)):
            GPIO.setup(self.COLUMN[j], GPIO.OUT)
            GPIO.output(self.COLUMN[j], GPIO.LOW)

        # Set all rows as input
        for i in range(len(self.ROW)):
            GPIO.setup(self.ROW[i], GPIO.IN, pull_up_down=GPIO.PUD_UP)

        # Scan rows for pushed key/button
        # A valid key press should set "rowVal"  between 0 and 3.
        rowVal = -1
        for i in range(len(self.ROW)):
            tmpRead = GPIO.input(self.ROW[i])
            if tmpRead == 0:
                rowVal = i
        # if rowVal is not 0 thru 3 then no button was pressed and we can exit
        if rowVal <0 or rowVal >3:
            self.exit()
            return

        if self.PULL_DOWN:
            # Convert columns to input
            for i in range(len(self.COLUMN)):
                GPIO.setup(self.COLUMN[i], GPIO.IN)

            # Switch the i-th row found from scan to output
            GPIO.setup(self.ROW[rowVal], GPIO.OUT)
            GPIO.output(self.ROW[rowVal], GPIO.LOW)

            # Scan columns for still-pushed key/button
            # A valid key press should set "colVal"  between 0 and 2.
            colVal = -1
            for i in range(len(self.COLUMN)):
                tmpRead = GPIO.input(self.COLUMN[i])
                if tmpRead == 0:
                    colVal = i

        else:
            # Convert columns to input
            for j in range(len(self.COLUMN)):
                GPIO.setup(self.COLUMN[j], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

            # Switch the i-th row found from scan to output
            GPIO.setup(self.ROW[rowVal], GPIO.OUT)
            GPIO.output(self.ROW[rowVal], GPIO.HIGH)

            # Scan columns for still-pushed key/button
            # A valid key press should set "colVal"  between 0 and 2.
            colVal = -1
            for j in range(len(self.COLUMN)):
                tmpRead = GPIO.input(self.COLUMN[j])
                if tmpRead == 1:
                    colVal=j

        # if colVal is not 0 thru 2 then no button was pressed and we can exit
        if colVal <0 or colVal >3:
            self.exit()
            return

        # Return the value of the key pressed
        self.exit()
        return self.KEYPAD[rowVal][colVal]

    def exit(self):
        # Reinitialize all rows and columns as input at exit
        for i in range(len(self.ROW)):
                GPIO.setup(self.ROW[i], GPIO.IN, pull_up_down=GPIO.PUD_UP)
        for j in range(len(self.COLUMN)):
                GPIO.setup(self.COLUMN[j], GPIO.IN, pull_up_down=GPIO.PUD_UP)
