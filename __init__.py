import os, re, sys 
from .utils.logger import log

if sys.version_info.major < 3:
    log.error(__name__ + " is only supported on Python 3")
    sys.exit(-1)

from .system.platform.beaglebone import info as BB
from .system.platform.raspberry  import info as RP
from .system.platform.raspberry2  import info as RP2
from .system.platform.cubieboard import info as CB

f = open('/proc/cpuinfo', 'r')
content = f.read()
f.close()

match = re.search( r'.*Hardware[\t ]*: (.*)\n.*Revision[\t ]*: (.*)', content, re.M|re.I)
__builtins__['HARDWARE'] = match.group(1)
__builtins__['REVISION'] = match.group(2)

if re.match(".*(" + ")|(".join(BB.HARDWARE) + ").*", HARDWARE, re.I):
    log.info("Detected BeagleBone platform")
    __builtins__['PLATFORM'] = "beaglebone"
elif re.match(".*(" + ")|(".join(RP.HARDWARE) + ").*", HARDWARE, re.I):
    log.info("Detected Raspberry PI platform")
    __builtins__['PLATFORM'] = "raspberry"
elif re.match(".*(" + ")|(".join(RP2.HARDWARE) + ").*", HARDWARE, re.I):
    log.info("Detected Raspberry PI 2 platform")
    __builtins__['PLATFORM'] = "raspberry"
elif re.match(".*(" + ")|(".join(CB.HARDWARE) + ").*", HARDWARE, re.I):
    log.info("Detected Cubieboard platform")
    __builtins__['PLATFORM'] = "cubieboard"
else:
    log.error("Unknown platform detected")
    sys.exit(-1)

