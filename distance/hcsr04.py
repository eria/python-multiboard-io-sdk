from ..system.io.gpio import *
import time

class HCSR04:
    
    def __init__(self, pinTrigger, pinEcho, timeout=3000):
        self.TRIGGER = GPIO(pinTrigger)
        self.ECHO    = GPIO(pinEcho)
        self.TIMEOUT = timeout/1000000
        
        self.TRIGGER.setup(OUT)
        self.ECHO.setup(IN)
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.stop()
        
    def stop(self):
        self.TRIGGER.stop() 
        self.ECHO.stop() 
            
    def time(self):
        self.TRIGGER.write(HIGH)
        time.sleep(0.0001)
        self.TRIGGER.write(LOW)
        
        duration = self.TIMEOUT
        start = time.time()
        stop  = time.time()
        while self.ECHO.read() == LOW and stop-start < self.TIMEOUT:
            stop  = time.time()
        
        if stop-start < self.TIMEOUT:
            start = time.time()
            stop  = time.time()
            while self.ECHO.read() == HIGH and stop-start < self.TIMEOUT:
                stop  = time.time()
            duration = time.time() - start
        
        return duration

    
    def distance(self, cm=True):
        duration = self.time() * 1000000
        if cm:
            return duration / 29 / 2
        else:
            return duration / 74 / 2
        
        
