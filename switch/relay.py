import time
import threading
from ..system.io.gpio import *
from ..utils.logger import log

class Relay:
    MODE = 0
    PINS = [4,25]
    STATUS = []
    BLOCKED = []

    def __init__(self, **kwargs):
        if "pins" in kwargs:
            self.PINS = list(map(int, kwargs['pins'].split(",")))
        if "mode" in kwargs:
            self.MODE = int(kwargs['mode'])

        log.debug("Initializing Relays: (Mode: %d, Pins: %s)" % (self.MODE,','.join(str(x) for x in self.PINS)))
        self.GPIO = []
        for pos,pin in enumerate(self.PINS):
            g = GPIO(pin)
            g.setup(OUT)
            self.GPIO.append(g)
            self.STATUS.append(False)
            self.BLOCKED.append(False)
            self.deactivate(pos)

    def isActive(self, rnum):
        if rnum < len(self.PINS):
            return self.STATUS[rnum]
        return False
    
    def activate(self, rnum):
        if rnum < len(self.PINS):
            while self.BLOCKED[rnum]: continue
            self.BLOCKED[rnum] = True
            log.info("Activating relay: %s" % self.PINS[rnum])
            self.STATUS[rnum] = True
            m = HIGH if self.MODE == 0 else LOW
            self.GPIO[rnum].write(m)
            self.BLOCKED[rnum] = False

            
    def deactivate(self, rnum):
        if rnum < len(self.PINS):
            while self.BLOCKED[rnum]: continue
            self.BLOCKED[rnum] = True
            log.info("Closing relay: %s" % self.PINS[rnum])
            self.STATUS[rnum] = False
            m = LOW if self.MODE == 0 else HIGH
            self.GPIO[rnum].write(m)
            self.BLOCKED[rnum] = False

    def activateDuring(self, rnum, secs):
        thread = threading.Thread(target=self.timed,args=(rnum, secs,))
        thread.start()

    def timed(self, rnum, secs):
        self.activate(rnum)
        time.sleep(secs)
        self.deactivate(rnum)
