from ..utils.logger import log
from ..utils.conversors import *

##
# Mifare Commands
##
MIFARE_CMD_AUTH_A       = 0x60
MIFARE_CMD_AUTH_B       = 0x61
MIFARE_CMD_READ         = 0x30
MIFARE_CMD_WRITE        = 0xA0
MIFARE_CMD_TRANSFER     = 0xB0
MIFARE_CMD_DECREMENT    = 0xC0
MIFARE_CMD_INCREMENT    = 0xC1
MIFARE_CMD_RESTORE      = 0xC2

##
# Default Keys
##
MIFARE_DEF_KEYS = [
    bytes([0xff, 0xff, 0xff, 0xff, 0xff, 0xff]),
    bytes([0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5]),
    bytes([0xd3, 0xf7, 0xd3, 0xf7, 0xd3, 0xf7]),
    bytes([0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
    bytes([0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5]),
    bytes([0x4d, 0x3a, 0x99, 0xc3, 0x51, 0xdd]),
    bytes([0x1a, 0x98, 0x2c, 0x7e, 0x45, 0x9a]),
    bytes([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]),
    bytes([0x71, 0x4c, 0x5c, 0x88, 0x6e, 0x97]),
    bytes([0x58, 0x7e, 0xe5, 0xf9, 0x35, 0x0f]),
    bytes([0xa0, 0x47, 0x8c, 0xc3, 0x90, 0x91]),
    bytes([0x53, 0x3c, 0xb6, 0xc7, 0x23, 0xf6]),
    bytes([0x8f, 0xd0, 0xa4, 0xf2, 0x56, 0xe9])
]

##
# Mifare Types & Sizes
##
MIFARE_TYPE_UNKNOWN = -1
MIFARE_TYPE_CLASSIC = 0
MIFARE_TYPE_PLUS    = 1
MIFARE_TYPE_PRO     = 2

MIFARE_SIZE_1K      = 1024
MIFARE_SIZE_2K      = 2048
MIFARE_SIZE_4K      = 4096
MIFARE_SIZE_MINI    = 320


##
# Block vars
##
MIFARE_BLOCK_SIZE       = 16
MIFARE_MAX_BLOCK_COUNT  = 256
MIFARE_MAX_SECTOR_COUNT = 40

##
# Block Types
##
MIFARE_TYPE_RW           = 0
MIFARE_TYPE_VALUE        = 1
MIFARE_TYPE_TRAILER      = 2
MIFARE_TYPE_MANUFACTURER = 3
	

##
# Permissions
##
MIFARE_PERM_STRINGS  = ["Never", "A", "B", "A|B"];

#R W WKA RKB WKB	
MIFARE_TRAILER_ACL = [
	[1, 0, 1, 1, 1],
	[1, 1, 1, 1, 1],
	[1, 0, 0, 1, 0],
	[3, 2, 2, 0, 2],
	[3, 0, 2, 0, 2],
	[3, 2, 0, 0, 0],
	[3, 0, 0, 0, 0],
	[3, 0, 0, 0, 0]
]
	
# R W I D
MIFARE_DATA_ACL = [
	[3, 3, 3, 3],
	[3, 0, 0, 3],
	[3, 0, 0, 0],
	[2, 2, 0, 0],
	[3, 2, 0, 0],
	[2, 0, 0, 0],
	[3, 2, 2, 3],
	[0, 0, 0, 0]
]


class MifareClassic:
    isEmulated = False;
    type = 0;
    size = 0;
    
    def __init__(self, tag):
        self.tag = tag
        
        if tag.sak == 0x01 or tag.sak == 0x08:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_1K;
        elif tag.sak == 0x09:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_MINI;
        elif tag.sak == 0x10:
            self.type = MIFARE_TYPE_PLUS;
            self.size = MIFARE_SIZE_2K;
        elif tag.sak == 0x11:
            self.type = MIFARE_TYPE_PLUS;
            self.size = MIFARE_SIZE_4K;
        elif tag.sak == 0x18:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_4K;
        elif tag.sak == 0x28:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_1K;
            self.isEmulated = True;
        elif tag.sak == 0x38:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_4K;
            self.isEmulated = True;
        elif tag.sak == 0x88:
            self.type = MIFARE_TYPE_CLASSIC;
            self.size = MIFARE_SIZE_1K;
        elif tag.sak == 0x98 or tag.sak == 0xB8:
            self.type = MIFARE_TYPE_PRO;
            self.size = MIFARE_SIZE_4K;
        else:
            raise NameError("Tag incorrectly enumerated as MIFARE Classic, SAK = %0.2x" % tag.sak)
    
    
    def getSectorCount(self):
        if self.size   == MIFARE_SIZE_1K:
            return 16
        elif self.size == MIFARE_SIZE_2K:
            return 32
        elif self.size == MIFARE_SIZE_4K:
            return 40
        elif self.size == MIFARE_SIZE_MINI:
            return 5
        else:
            return 0
            
    def getBlockCount(self):
        return self.size / MIFARE_BLOCK_SIZE;
    
   
    def getBlockCountInSector(self, sectorIndex):
        self.__validateSector(sectorIndex)

        if sectorIndex < 32:
            return 4
        else:
            return 16

    def blockToSector(self, blockIndex):
        self.__validateBlock(blockIndex)

        if blockIndex < 32 * 4:
            return blockIndex / 4
        else:
            return 32 + (blockIndex - 32 * 4) / 16        

    def sectorToBlock(self, sectorIndex):
        if sectorIndex < 32:
            return sectorIndex * 4
        else:
            return 32 * 4 + (sectorIndex - 32) * 16
            
    
    def __validateSector(self, sector):
        if sector < 0 or sector >= MIFARE_MAX_SECTOR_COUNT:
            raise NameError("Sector out of bounds: %d" % sector)
            
    def __validateBlock(self, block):
        if block < 0 or block >= MIFARE_MAX_BLOCK_COUNT:
            raise NameError("block out of bounds: %d" % block)

    def __validateValueOperand(self, value):
        if value < 0:
            raise NameError("value operand negative")

    
    def authenticateSectorWithKeyA(self, sectorIndex, key):
        return self.authenticate(sectorIndex, key, 0);
        
    def authenticateSectorWithKeyB(self, sectorIndex, key):
        return self.authenticate(sectorIndex, key, 1);
   
    def authenticate(self, sectorIndex, key, keyType=0):
        log.debug("Mifare Authentication: cardId [%s], sector [%d], key [%s], keyType [%d]" % 
            (hexdump(self.tag.id), sectorIndex, hexdump(key), keyType))
        self.__validateSector(sectorIndex)
            
        byte_array = bytearray()

        byte_array.append(0x01)                      # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_AUTH_A+keyType) # key type A=0 B=1
        byte_array.append(sectorIndex)               # sector number
        for k in key:         byte_array.append(k)   # key
        for c in self.tag.id: byte_array.append(c)   # CardId  
            
        self.tag.send(byte_array)
        resp = self.tag.read()
        
    def readBlock(self, blockIndex):
        log.debug("Mifare read block %d" % blockIndex)
        self.__validateBlock(blockIndex)
        
        byte_array = bytearray()

        byte_array.append(0x01)            # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_READ) # Command for READ
        byte_array.append(blockIndex)      # block number
        
        self.tag.send(byte_array)
        return self.tag.read()

        
    def writeBlock(self, blockIndex, data):
        log.debug("Mifare write block %d: [%s]" % (blockIndex, hexdump(data)))
        self.__validateBlock(blockIndex)

        if len(data) != MIFARE_BLOCK_SIZE: 
            log.error("Invalid data length")
            return False

        byte_array = bytearray()

        byte_array.append(0x01)             # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_WRITE) # Command for WRITE
        byte_array.append(blockIndex)       # block number
        for d in data: byte_array.append(d) # data
        
        self.tag.send(byte_array)
        return self.tag.read()
        
        
    def increment(self, blockIndex, value):
        log.debug("Mifare increment block %d: [%s]" % (blockIndex, value))
        self.__validateBlock(blockIndex)
        
        byte_array = bytearray()

        byte_array.append(0x01)                 # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_INCREMENT) # Command for INCREMENT
        byte_array.append(blockIndex)           # block number
        byte_array.append(value)                # value
        
        self.tag.send(byte_array)
        return self.tag.read()
        
        
    def decrement(self, blockIndex, value):
        log.debug("Mifare decrement block %d: [%s]" % (blockIndex, value))
        self.__validateBlock(blockIndex)
        
        byte_array = bytearray()

        byte_array.append(0x01)                 # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_DECREMENT) # Command for DECREMENT
        byte_array.append(blockIndex)           # block number
        byte_array.append(value)                # value
        
        self.tag.send(byte_array)
        return self.tag.read()
       
        
    def transfer(self, blockIndex):
        log.debug("Mifare transfer block %d" % blockIndex)
        self.__validateBlock(blockIndex)
        
        byte_array = bytearray()

        byte_array.append(0x01)                 # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_TRANSFER)  # Command for TRANSFER
        byte_array.append(blockIndex)           # block number
        
        self.tag.send(byte_array)
        return self.tag.read()
        
    
    def restore(self, blockIndex):
        log.debug("Mifare restore block %d" % blockIndex)
        self.__validateBlock(blockIndex)
        
        byte_array = bytearray()

        byte_array.append(0x01)                 # logical number of the relevant target (1 or 0x01 ?)
        byte_array.append(MIFARE_CMD_RESTORE)   # Command for RESTORE
        byte_array.append(blockIndex)           # block number
        
        self.tag.send(byte_array)
        return self.tag.read()   
   
    

    def getTypeString(self):
        if self.type == 0:
            return "Mifare Classic"
        elif self.type == 1:
            return "Mifare Plus"
        elif self.type == 2:
            return "Mifare Pro"
        else:
            return "Unknown"

    def getSizeString(self):
        if self.size >= 1024:
            return "%dK" % (self.size/1024)
        else: 
            self.size  
            
            
    # Block Info
    
    def getBlockType(self, blockIndex):
        sector = self.blockToSector(blockIndex)
        if blockIndex == 0: 
            return MIFARE_TYPE_MANUFACTURER
        elif (blockIndex+1) == self.getBlockCountInSector(sector):
            return MIFARE_TYPE_TRAILER
        else:
            return MIFARE_TYPE_VALUE


    def getBlockInfo(self, trailer, blockIndex):
        type = self.getBlockType(blockIndex)
		
        if type == MIFARE_TYPE_TRAILER:
            return self.getTrailerInfo(trailer);
        else:
            return self.getDataInfo(trailer, blockIndex);
            
	
    def getTrailerInfo(self, trailer):
        bin = self.getBlockACL(trailer, 3)
        
        return [ 1,
            MIFARE_PERM_STRINGS[MIFARE_TRAILER_ACL[bin][0]],
            MIFARE_PERM_STRINGS[MIFARE_TRAILER_ACL[bin][1]],
            MIFARE_PERM_STRINGS[0],
            MIFARE_PERM_STRINGS[MIFARE_TRAILER_ACL[bin][2]],
            MIFARE_PERM_STRINGS[MIFARE_TRAILER_ACL[bin][3]],
            MIFARE_PERM_STRINGS[MIFARE_TRAILER_ACL[bin][4]]
        ]
        

	
    def getDataInfo(self, trailer, blockIndex):
        bin = self.getBlockACL(trailer, blockIndex)
        
        return [ 0,
            MIFARE_PERM_STRINGS[MIFARE_DATA_ACL[bin][0]],
            MIFARE_PERM_STRINGS[MIFARE_DATA_ACL[bin][1]],
            MIFARE_PERM_STRINGS[MIFARE_DATA_ACL[bin][2]],
            MIFARE_PERM_STRINGS[MIFARE_DATA_ACL[bin][3]]
        ]

    def getBlockACL(self, trailer, blockIndex):
        db1 = 1 << blockIndex;
        db2 = 1 << (4+blockIndex);

        bin = 0x00;
        if (trailer[7] & db2) != 0: bin += 4  
        if (trailer[8] & db1) != 0: bin += 2
        if (trailer[8] & db2) != 0: bin += 1
        return bin
    
