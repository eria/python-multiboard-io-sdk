import time
import serial
import binascii
import errno
import os
import sys
from ..Tag import Tag
from struct import pack, unpack
from ...utils.logger import log
from ...system.io.gpio import *



# from ...system.io.uart import UART
# from ..Tag import Tag
# from ...utils.logger import log
# from ...utils.conversors import *

##
# PN532 COMMANDS
##

# Miscellaneous
PN532_CMD_DIAGNOSE              = 0x00
PN532_CMD_GETFIRMWAREVERSION    = 0x02
PN532_CMD_GETGENERALSTATUS      = 0x04
PN532_CMD_READREGISTER          = 0x06
PN532_CMD_WRITEREGISTER         = 0x08
PN532_CMD_READGPIO              = 0x0C
PN532_CMD_WRITEGPIO             = 0x0E
PN532_CMD_SETSERIALBAUDRATE     = 0x10
PN532_CMD_SETPARAMETERS         = 0x12
PN532_CMD_SAMCONFIGURATION      = 0x14
PN532_CMD_POWERDOWN             = 0x16

# RF COMM
PN532_CMD_RFCONFIGURATION       = 0x32
PN532_CMD_RFREGULATIONTEST      = 0x58

# INITIATOR
PN532_CMD_INJUMPFORDEP          = 0x56
PN532_CMD_INJUMPFORPSL          = 0x46
PN532_CMD_INLISTPASSIVETARGET   = 0x4A
PN532_CMD_INATR                 = 0x50
PN532_CMD_INPSL                 = 0x4E
PN532_CMD_INDATAEXCHANGE        = 0x40
PN532_CMD_INCOMMUNICATETHRU     = 0x42
PN532_CMD_INDESELECT            = 0x44
PN532_CMD_INRELEASE             = 0x52
PN532_CMD_INSELECT              = 0x54
PN532_CMD_INAUTOPOLL            = 0x60

# TARGET
PN532_CMD_TGINITASTARGET        = 0x8C
PN532_CMD_SETGENERALBYTES       = 0x92
PN532_CMD_TGGETDATA             = 0x86
PN532_CMD_TGSETDATA             = 0x8E
PN532_CMD_TGSETMETADATA         = 0x94
PN532_CMD_TGGETINITIATORCOMMAND = 0x88
PN532_CMD_TGRESPONSETOINITIATOR = 0x90
PN532_CMD_TGGETTARGETSTATUS     = 0x8A


##
# PN532 FRAME CONSTANTS
##

PN532_FRAME_PREAMBLE            = 0x00
PN532_FRAME_STARTCODE1          = 0x00
PN532_FRAME_STARTCODE2          = 0xFF
PN532_FRAME_POSTAMBLE           = 0x00


##
# PN532 FRAME IDENTIFIERS
##

PN532_IDENTIFIER_HOST_TO_PN532 = 0xD4
PN532_IDENTIFIER_PN532_TO_HOST = 0xD5


##
# PN532 SAM MODES
##
PN532_SAM_NORMAL_MODE          = 0x01
PN532_SAM_VIRTUAL_CARD         = 0x02
PN532_SAM_WIRED_CARD           = 0x03
PN532_SAM_DUAL_CARD            = 0x04

##
# PN532 BAUD RATES
##
PN532_BRTY_ISO14443A           = 0x00
PN532_BRTY_212KBPS             = 0x01
PN532_BRTY_424KBPS             = 0x02
PN532_BRTY_ISO14443B           = 0x03
PN532_BRTY_JEWEL               = 0x04

class PN532Frame:
    _identifier = PN532_IDENTIFIER_HOST_TO_PN532
    _command    = 0x00
    _data       = ""
    _ack        = False
    
    def __init__(self, 
        identifier=PN532_IDENTIFIER_HOST_TO_PN532,
        command=PN532_CMD_GETFIRMWAREVERSION, 
        data=bytearray(), ack=False):
        
        self._identifier = identifier
        self._command    = command
        self._data       = data
        self._ack        = ack
        
    def getFrame(self):
        byte_array = bytearray()
        
        byte_array.append(PN532_FRAME_PREAMBLE)
        byte_array.append(PN532_FRAME_STARTCODE1)
        byte_array.append(PN532_FRAME_STARTCODE2)
        
        if self._ack:        
            byte_array.append(PN532_FRAME_STARTCODE1)
            byte_array.append(PN532_FRAME_STARTCODE2)
        else:
            byte_array.append(self.getLength())
            byte_array.append(self.getLengthCRC())
            byte_array.append(self._identifier)
            byte_array.append(self._command)
            for byte in self._data: byte_array.append(byte)
            byte_array.append(self.getDataCRC())
            
        byte_array.append(PN532_FRAME_POSTAMBLE)

        return (byte_array)

    def getAsciiFrame(self):
        return (binascii.hexlify(self.getFrame()))

    def getData(self):
        return self._data
        
    def getIdentifier(self):
        return self._identifier
        
    def getCommand(self):
        return self._command
        
    def isACK(self):
        return self._ack;


    def getLength(self):
        return len(self._data) + 2 # Identifier + Command + Data

    def getLengthCRC(self):
        return (~self.getLength() & 0xFF) + 0x01

    def getDataCRC(self):
        byte_array = bytearray()

        byte_array.append(self._identifier)
        byte_array.append(self._command)
        for byte in self._data: byte_array.append(byte)

        inverse = (~sum(byte_array) & 0xFF) + 0x01
        if inverse > 255: inverse = inverse - 255

        return inverse
                
        
    @staticmethod
    def fromResponse(response):
        if PN532Frame.isValid(response) is not True:
            raise RuntimeError("Invalid Response")

        if PN532Frame.isAck(response):
            return PN532Frame(ack=True)

        length = response[0][4]
        data = bytearray(response[0][8:8+length-2])

        return PN532Frame(identifier=response[0][6], command=response[0][7], data=data)

    @staticmethod
    def isValid(response):
        if (response[0][0] & 0x01) == 0x01:
            if response[0][1] == PN532_FRAME_PREAMBLE:
                if response[0][2] == PN532_FRAME_STARTCODE1:
                    if response[0][3] == PN532_FRAME_STARTCODE2:
                        return True

        return False
        
        
    @staticmethod
    def isAck(response):
        if response[0][4] == 0x00 and response[0][5] == 0xFF and response[0][6] == 0x00:
            return True

        return False



class PN532(object):
    def SAMConfigure(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def InListPassiveTarget(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def sendAndCheckACK(self, command, data):
        raise NotImplementedError( "Should have implemented this" )
        
    def send(self, command, data):
        raise NotImplementedError( "Should have implemented this" )
    
    def read(self):
        raise NotImplementedError( "Should have implemented this" )
    
    def sendACK(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def readACK(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def reconnect(self):
        raise NotImplementedError( "Should have implemented this" )
    
class PN532UART(PN532):
    PORT     = '/dev/ttyAMA0'
    BAUDRATE = 115200
    TIMEOUT  = 0.05
    UART = None
    VCC_CONTROL = 0
    VCC_CONTROL_MODE = 1
    VCC_CONTROL_PIN = 7

    ACK=b'\x00\x00\xFF\x00\xFF\x00'
    SOF=b'\x00\x00\xFF'
    power_down_wakeup_src = ("INT0","INT1","rfu","RF","HSU","SPI","GPIO","I2C")
    host_command_frame_max_size = 265
    in_list_passive_target_max_target = 2
    in_list_passive_target_brty_range = (0, 1, 2, 3, 4)
    REG = {
        0x6331: "CIU_Command",
        0x6332: "CIU_CommIEn",
        0x6333: "CIU_DivIEn",
        0x6334: "CIU_CommIRq",
        0x6335: "CIU_DivIRq",
        0x6336: "CIU_Error",
        0x6337: "CIU_Status1",
        0x6338: "CIU_Status2",
        0x6339: "CIU_FIFOData",
        0x633A: "CIU_FIFOLevel",
        0x633B: "CIU_WaterLevel",
        0x633C: "CIU_Control",
        0x633D: "CIU_BitFraming",
        0x633E: "CIU_Coll",
        0x6301: "CIU_Mode",
        0x6302: "CIU_TxMode",
        0x6303: "CIU_RxMode",
        0x6304: "CIU_TxControl",
        0x6305: "CIU_TxAuto",
        0x6306: "CIU_TxSel",
        0x6307: "CIU_RxSel",
        0x6308: "CIU_RxThreshold",
        0x6309: "CIU_Demod",
        0x630A: "CIU_FelNFC1",
        0x630B: "CIU_FelNFC2",
        0x630C: "CIU_MifNFC",
        0x630D: "CIU_ManualRCV",
        0x630E: "CIU_TypeB",
        0x630F: "CIU_SerialSpeed",
        0x6311: "CIU_CRCResultMSB",
        0x6312: "CIU_CRCResultLSB",
        0x6313: "CIU_GsNOff",
        0x6314: "CIU_ModWidth",
        0x6315: "CIU_TxBitPhase",
        0x6316: "CIU_RFCfg",
        0x6317: "CIU_GsNOn",
        0x6318: "CIU_CWGsP",
        0x6319: "CIU_ModGsP",
        0x631A: "CIU_TMode",
        0x631B: "CIU_TPrescaler",
        0x631C: "CIU_TReloadHi",
        0x631D: "CIU_TReloadLo",
        0x631E: "CIU_TCounterHi",
        0x631F: "CIU_TCounterLo",
        0x6321: "CIU_TestSel1",
        0x6322: "CIU_TestSel2",
        0x6323: "CIU_TestPinEn",
        0x6324: "CIU_TestPinValue",
        0x6325: "CIU_TestBus",
        0x6326: "CIU_AutoTest",
        0x6327: "CIU_Version",
        0x6328: "CIU_AnalogTest",
        0x6329: "CIU_TestDAC1",
        0x632A: "CIU_TestDAC2",
        0x632B: "CIU_TestADC",
        0x632C: "CIU_RFT1",
        0x632D: "CIU_RFT2",
        0x632E: "CIU_RFT3",
        0x632F: "CIU_RFT4",
    }

    REGBYNAME = {}
    for k in REG: REGBYNAME.update({REG[k] : k})
    
    def __init__(self, kwargs, supply=False):
        try:
            if "port" in kwargs:
                self.PORT = kwargs['port']
            if "baudrate" in kwargs:
                self.BAUDRATE = int(kwargs['baudrate'])
            # if "timeout" in kwargs:
            #     self.TIMEOUT = float(kwargs['timeout'])
            if "vcc_control" in kwargs:
                self.VCC_CONTROL = int(kwargs['vcc_control'])
            if "vcc_control_mode" in kwargs:
                self.VCC_CONTROL_MODE = int(kwargs['vcc_control_mode'])
            if "vcc_control_pin" in kwargs:
                self.VCC_CONTROL_PIN = int(kwargs['vcc_control_pin'])
                         
            log.info("Initializing NFC: (PORT: %s, VCC_CONTROL: %d, VCC_CONTROL_MODE: %d, VCC_CONTROL_PIN: %d, BAUDRATE: %d, TIMEOUT: %f)" % (self.PORT, self.VCC_CONTROL, self.VCC_CONTROL_MODE, self.VCC_CONTROL_PIN, self.BAUDRATE, self.TIMEOUT))
            
            if supply and self.VCC_CONTROL: 
                self.connect_supply()
            
            self.UART = serial.Serial(port=self.PORT, baudrate=self.BAUDRATE, timeout=self.TIMEOUT)

            # Init pn532
            baudrate = 115200 # PN532 initial baudrate
            long_preamble = bytearray(10)
            initial_timeout = 100

            get_version_cmd = bytearray.fromhex("0000ff02fed4022a00")
            get_version_rsp = bytearray.fromhex("0000ff06fad50332")
        
            self.write_frame(long_preamble + get_version_cmd)
            log.debug("wait %d ms for data on %s", initial_timeout, self.PORT)
            if not self.read_frame(timeout=initial_timeout) == self.ACK:
                raise IOError(errno.ENODEV, os.strerror(errno.ENODEV))
            if not self.read_frame(timeout=100).startswith(get_version_rsp):
                raise IOError(errno.ENODEV, os.strerror(errno.ENODEV))

            sam_configuration_cmd = bytearray.fromhex("0000ff05fbd4140100001700")
            sam_configuration_rsp = bytearray.fromhex("0000ff02fed5151600")
            self.write_frame(long_preamble + sam_configuration_cmd)
            if not self.read_frame(timeout=100) == self.ACK:
                raise IOError(errno.ENODEV, os.strerror(errno.ENODEV))
            if not self.read_frame(timeout=100) == sam_configuration_rsp:
                raise IOError(errno.ENODEV, os.strerror(errno.ENODEV))

            if sys.platform.startswith("linux"):
                stty = 'stty -F %s %%d 2> /dev/null' % self.PORT
                for baudrate in (921600, 460800, 230400, 115200):
                    log.debug("trying to set %d baud", baudrate)
                    if os.system(stty % baudrate) == 0:
                        os.system(stty % 115200)
                        break

            self.Diagnose('line')
            self.GetFirmwareVersion()
            log.debug("Test de arranque pasados, configurando...")
            self.SetParameters(0b00000000)
            self.RFConfiguration(0x02, [0x00,0x0B,0x0A])
            self.RFConfiguration(0x04, [0x00])
            self.RFConfiguration(0x05, [0x01,0x00,0x01])

            log.debug("write analog settings for Type A 106 kbps")
            self.RFConfiguration(0x0A, [0x59, 0xF4, 0x3F, 0x11, 0x4D, 0x85, 0x61, 0x6F, 0x26, 0x62, 0x87])
            
            # log.debug("write analog settings for Type F 212/424 kbps")
            # self.RFConfiguration(0x0B, [0x69, 0xFF, 0x3F, 0x11, 0x41, 0x85, 0x61, 0x6F])

            # log.debug("write analog settings for type B")
            # self.RFConfiguration(0x0C, [0xFF, 0x10, 0x85])

            # log.debug("write analog settings for 14443-4 212/424/848 kbps")
            # self.RFConfiguration(0x0D, [0x85, 0x15, 0x8A, 0x85, 0x08, 0xB2, 0x85, 0x01, 0xDA])

            self.Mute()
            log.info("NFC device started succesful")
        except:
            log.error("Error starting NFC device or by passing tests", exc_info=True)
            self.close()
        
    def connect_supply(self):
        if not self.VCC_CONTROL: 
            log.debug("Supply is allways ON, cannot be switched")
            return True
        try:
            GPIO_VCC = GPIO(self.VCC_CONTROL_PIN)
            GPIO_VCC.setup(OUT)
            if GPIO_VCC:
                log.info("Connecting NFC supply, pin: %d" % self.VCC_CONTROL_PIN)
                print(self.VCC_CONTROL_MODE)
                if self.VCC_CONTROL_MODE == 0:
                    GPIO_VCC.stop()
                    time.sleep(1)
                elif self.VCC_CONTROL_MODE == 1:
                    GPIO_VCC.write(LOW)
                else:
                    raise
            else:
                log.error("Supply control cannot be controlled")
        except:
            log.error("NFC cannot be plugged")
            raise
            
    def disconnect_supply(self):
        if not self.VCC_CONTROL: 
            log.debug("Supply is allways ON, cannot be switched")
            return True
        try:
            GPIO_VCC = GPIO(self.VCC_CONTROL_PIN)
            GPIO_VCC.setup(OUT)
            if GPIO_VCC:
                log.info("Disconnecting NFC supply")
                print(self.VCC_CONTROL_MODE)
                if self.VCC_CONTROL_MODE == 0:
                    GPIO_VCC.write(LOW)
                elif self.VCC_CONTROL_MODE == 1:
                    GPIO_VCC.write(HIGH)
                else:
                    raise
            else:
                log.error("Supply control cannot be controlled")
        except:
            log.error("NFC cannot be unplugged")
            raise

    def read_frame(self, timeout):
        if self.UART is not None:
            self.UART.timeout = max(timeout/1E3, 0.05)
            frame = bytearray(self.UART.read(6))
            if frame is None or len(frame) == 0:
                log.debug("Error reading frame")
                raise IOError(errno.ETIMEDOUT, os.strerror(errno.ETIMEDOUT))
            if frame.startswith(b'\x00\x00\xff\x00\xff\x00'):
                #log.debug("<<< %s" % binascii.hexlify(frame))
                return frame
            LEN = frame[3]
            if LEN == 0xFF:
                frame += self.UART.read(3)
                LEN = frame[5]<<8 | frame[6]
            frame += self.UART.read(LEN + 1)
            #log.debug("<<< %s" % binascii.hexlify(frame))
            return frame

    def write_frame(self, frame):
        if self.UART is not None:
            #log.debug(">>> %s" % binascii.hexlify(frame))
            self.UART.flushInput()
            try:
                self.UART.write(frame)
            except serial.SerialTimeoutException:
                log.debug("Error writing frame")
                raise IOError(errno.EIO, os.strerror(errno.EIO))

    def command(self, cmd_code, cmd_data, timeout):
        """
        Send a host command and return the chip response. The chip command
        is selected by the 8-bit integer *cmd_code*. The command
        parameters, if any, are supplied with *cmd_data* as a
        bytearray or byte string. The fully constructed command frame
        is sent with :meth:`write_frame` and the chip acknowledgement
        and response is received with :meth:`read_frame`.

        The implementation waits 100 ms for the command acknowledgement
        and then polls every 100 ms for a response frame until
        *timeout* seconds have elapsed. If the response frame is
        correct and the response code matches *cmd_code* the data
        bytes that follow the response code are returned as a
        bytearray (without the trailing checksum and postamble).

        **Exceptions**
        * :exc:`~exceptions.IOError` :const:`errno.ETIMEDOUT` if no
          response frame was received before *timeout* seconds.
        * :exc:`~exceptions.IOError` :const:`errno.EIO` if response
          frame errors were detected.
        * :exc:`Chipset.Error` if an error response frame or status
          error was received.
        """
        #log.debug(hex(cmd_code)[2:].zfill(2), binascii.hexlify(cmd_data))
        try:
            if cmd_data is not None:
                req_frame = PN532Frame(command=cmd_code, data=cmd_data)
                try:
                    self.write_frame(req_frame.getFrame())
                    resp_frame = self.read_frame(timeout=100)
                except IOError as error:
                    log.debug("Input/Output error while waiting for ack")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                if not resp_frame.startswith(self.SOF):
                    log.debug("Invalid frame start sequence")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                if resp_frame[0:len(self.ACK)] != self.ACK:
                    log.debug("Missing ack frame")
            else:
                frame = self.ACK
            
            if timeout is not None and timeout <= 0:
                return
            
            while resp_frame == self.ACK:
                try:
                    resp_frame = self.read_frame(int(1000 * timeout))
                except IOError as error:
                    if error.errno == errno.ETIMEDOUT:
                        self.write_frame(self.ACK) # cancel command
                        time.sleep(0.001)
                    raise error

            if resp_frame.startswith(self.SOF + b'\xFF\xFF'):
                #log.debug("Extended frame")
                if sum(resp_frame[5:8]) & 0xFF != 0:
                    log.debug("Frame lenght checksum error")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                if unpack(">H", buffer(resp_frame[5:7]))[0] != len(resp_frame) - 10:
                    log.debug("Frame lenght value mismatch")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                del resp_frame[0:8]
            elif resp_frame.startswith(self.SOF):
                #log.debug("Normal frame")
                if sum(resp_frame[3:5]) & 0xFF != 0:
                    log.debug("Frame lenght checksum error")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                if resp_frame[3] != len(resp_frame) - 7:
                    log.debug("Frame lenght value mismatch")
                    raise IOError(errno.EIO, os.strerror(errno.EIO))
                del resp_frame[0:5]
            else:
                log.debug("Invalid frame start sequence")
                raise IOError(errno.EIO, os.strerror(errno.EIO))

            if not sum(resp_frame[0:-1]) & 0xFF == 0:
                log.debug("Frame data checksum error")
                raise IOError(errno.EIO, os.strerror(errno.EIO))

            if resp_frame[0] == 0x7F: # error frame
                log.debug("Error frame!")
                self.chipset_error(0x7F)

            if not resp_frame[0] == 0xD5:
                log.debug("Invalid frame identifier")
                raise IOError(errno.EIO, os.strerror(errno.EIO))

            if not resp_frame[1] == cmd_code + 1:
                log.debug("Unexpected response code")
                raise IOError(errno.EIO, os.strerror(errno.EIO))

            return resp_frame[2:-2]
        except:
            log.debug("Error sending command")
            #log.error("Error sending command", exc_info=True)

    #Public methods
    def Diagnose(self, test, test_data=None):
        """Send a Diagnose command. The *test* argument selects the diagnose
        function either by number or the string ``line``, ``rom``, or
        ``ram``. For a ``line`` test the implementation sends the
        longest possible command frame and verifies that the response
        data is identical. For a ``ram`` or ``rom`` test the
        implementation verfies the response status. For a *test*
        number the implementation appends the byte string *test_data*
        and returns the response data bytes.
        """
        if test == "line":
            size = self.host_command_frame_max_size - 13
            data = bytearray([0x00]) + bytearray([x&0xFF for x in range(size)])
            return self.command(cmd_code=0x00, cmd_data=data, timeout=1.0) == data
        if test == "rom":
            data = self.command(cmd_code=0x00, cmd_data=bytearray([0x01]), timeout=1.0)
            return data and data[0] == 0
        if test == "ram":
            data = self.command(cmd_code=0x00, cmd_data=bytearray([0x02]), timeout=1.0)
            return data and data[0] == 0
        return self.command(0x00, chr(test), test_data, timeout=1.0)

    def RFConfiguration(self, cfg_item, cfg_data):
        self.command(cmd_code=0x32, cmd_data=bytearray([cfg_item] + cfg_data), timeout=0.1)

    def SetParameters(self, flags):
        self.command(cmd_code=0x12, cmd_data=bytearray([flags]), timeout=0.1)

    def ReadRegister(self, *args):
        """Send a ReadRegister command for the positional register address or
        name arguments. The register values are returned as a list for
        multiple arguments or an integer for a single argument. ::
          tx_mode = Chipset.ReadRegister(0x6302)
          rx_mode = Chipset.ReadRegister("CIU_RxMode")
          tx_mode, rx_mode = Chipset.ReadRegister("CIU_TxMode", "CIU_RxMode")
        """
        
        addr = lambda r: self.REGBYNAME[r] if type(r) is str else r
        args = [addr(reg) for reg in args]
        data=bytearray()
        for reg in args: 
            data+=pack(">H", reg)
        data = self.command(cmd_code=0x06, cmd_data=data, timeout=0.25)
        return list(data) if len(data) > 1 else data[0]

    def WriteRegister(self, *args):
        """Send a WriteRegister command. Each positional argument must be an
        (address, value) tuple except if exactly two arguments are
        supplied as register address and value. A register can also be
        selected by name. There is no return value. ::
          Chipset.WriteRegister(0x6301, 0x00)
          Chipset.WriteRegister("CIU_Mode", 0x00)
          Chipset.WriteRegister((0x6301, 0x00), ("CIU_TxMode", 0x00))
        """
        assert type(args) in (tuple, list)
        if len(args) == 2 and type(args[1]) == int: args = [args]
        addr = lambda r: self.REGBYNAME[r] if type(r) is str else r
        args = [(addr(reg), val) for reg, val in args]
        data=bytearray()
        for reg, val in args: 
            data+=pack(">HB", reg, val)
        self.command(cmd_code=0x08, cmd_data=data, timeout=0.25)

    def GetFirmwareVersion(self):
        resp_frame = self.command(cmd_code=PN532_CMD_GETFIRMWAREVERSION, cmd_data=bytearray(), timeout=0.1)
        raw_firm=str(binascii.hexlify(resp_frame))
        log.debug("Firmware version: "+raw_firm)
        return raw_firm

    def Mute(self):
        self.RFConfiguration(0x01, [0b00000010])

    def PowerDown(self, wakeup_enable, generate_irq=False):
        wakeup_set = 0
        for i, src in enumerate(self.power_down_wakeup_src):
            if src in wakeup_enable: wakeup_set |= 1 << i
        cmd_data = bytearray([wakeup_set, int(generate_irq)])
        data = self.command(0x16, cmd_data, timeout=0.1)
        # if data[0] != 0: log.debug("Error sending PowerDown")

    def InRelease(self):
        self.command(cmd_code=0x52, cmd_data=bytearray([0x00]), timeout=0.35)

    def SetSerialBaudrate(self, baudrate):
        br = (9600,19200,38400,57600,115200,230400,460800,921600,1288000)
        self.command(cmd_code=0x10, cmd_data=bytearray(bytes([br.index(baudrate)])), timeout=0.1)
        self.write_frame(self.ACK)
        time.sleep(0.001)

    def InListPassiveTarget(self, max_tg, brty, initiator_data):
        assert max_tg <= self.in_list_passive_target_max_target
        assert brty in self.in_list_passive_target_brty_range
        #data = 0x01 + chr(brty) + initiator_data
        data = bytearray(bytes([1])+bytes([brty])) + initiator_data
        #('InList', bytearray(b'\x01\x00\xa1-\xc1D'))
        data = self.command(cmd_code=0x4A, cmd_data=data, timeout=1.0)
        return data[2:] if data and data[0] > 0 else None

    def SenseTTA(self, target=None):
        try:
            if target != None: target=target
            else: target = { 'brty': '106A' }
            # target: 106A sdd_res=A12DC144 sel_req=A12DC144 sel_res=08 sens_res=0400
            if(target['brty'] == '106A'): brty=0
            if brty not in self.in_list_passive_target_brty_range:
                message = "unsupported bitrate {0}".format(target.brty)
                log.debug(message); raise ValueError(message)

            uid = target['sel_req'] if 'sel_req' in target else bytearray()
            if len(uid) > 4: uid = "\x88" + uid
            if len(uid) > 8: uid = uid[0:4] + "\x88" + uid[4:]

            rsp = self.InListPassiveTarget(1, 0, uid)
            if rsp is not None:
                sens_res, sel_res, sdd_res = rsp[1::-1], rsp[2:3], rsp[4:]
                t=Tag(self, rsp[1::-1], rsp[2:3], rsp[4:])
                if sel_res[0] & 0x60 == 0x00:
                    #log.debug("disable crc check for type 2 tag")
                    rxmode = self.ReadRegister("CIU_RxMode")
                    self.WriteRegister("CIU_RxMode", rxmode & 0x7F)
                return t
                #return {'brty' : "106A", 'sens_res' : sens_res, 'sel_res' : sel_res, 'sdd_res' : sdd_res}

            if self.ReadRegister("CIU_FIFOData") == 0x26:
                # If we still see the SENS_REQ command in the CIU FIFO
                # then there was no SENS_RES, thus no tag present.
                return None
        except:
            log.debug("Error doing SenseTTA")
            raise

   
    def reconnect(self, args):
        self.close()
        self.__init__(args)

    def close(self):
        if self.UART is not None:
            self.InRelease()
            # Terminate last chip command in case we've interrupted befor
            # the response and give the chip 10 ms to think about.
            self.write_frame(self.ACK)
            time.sleep(0.01)

            # When using the high speed uart we must set the baud rate
            # back to 115.2 kbps, otherwise we can't talk next time.
            self.SetSerialBaudrate(115200)

            # Set the chip to sleep mode with some wakeup sources.
            self.PowerDown(wakeup_enable=("I2C", "SPI", "HSU"))
            self.UART.flushOutput()
            self.UART.close()
            self.UART = None

    def __exit__(self, type, value, traceback):
        self.close()
        del self.UART

