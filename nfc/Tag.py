
class Tag:
    
    def __init__(self, service, atqa, sak, id):
        self.service = service
        self.atqa    = atqa
        self.sak     = sak
        self.id      = id
        
    def send(self, data, waitACK=True):
        if waitACK:
            return self.service.sendAndCheckACK(0x40, data)
        else:
            return self.service.send(0x40, data)
            
    def read(self):
        resp = self.service.read()
        if resp.getCommand() != (0x41):
            log.error("Authentication failed")
            return False
        
        return resp.getData()[1:]
    
    def isconnected(self):
        return
        
    def reconnect(self):
        self.service.reconect()