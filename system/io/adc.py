from .commons import *
from ...utils.logger import log

try:
    exec("from ..platform." + PLATFORM + ".io.adc import ADC")
except:
    log.info("ADC interface not implemented for %s" % PLATFORM)
    from ..platform.default.io.adc import ADC
