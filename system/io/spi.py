from .commons import *
from ...utils.logger import log

try:
    exec("from ..platform." + PLATFORM + ".io.spi import SPI")
except:
    log.info("SPI interface not implemented for %s" % PLATFORM)
    from ..platform.default.io.spi import SPI
