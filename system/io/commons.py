HIGH     = 1
LOW      = 0
OUT      = "out"
IN       = "in"

PUD_OFF  = 0
PUD_DOWN = 1
PUD_UP   = 2
RISING   = 1
FALLING  = 0
BOTH     = 2
