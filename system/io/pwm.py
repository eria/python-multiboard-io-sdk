from .commons import *
from ...utils.logger import log

try:
    exec("from ..platform." + PLATFORM + ".io.pwm import PWM")
except:
    log.info("PWM interface not implemented for %s" % PLATFORM)
    from ..platform.default.io.pwm import PWM
