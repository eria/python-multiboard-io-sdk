from .commons import *
from ...utils.logger import log

try:
    #GPIO_ = __import__("..platform." + PLATFORM + ".GPIO", fromlist= ["*"])
    exec("from ..platform." + PLATFORM + ".io.gpio import GPIO")
except:
    log.warn("GPIO interface not implemented for %s, loading defaults" % PLATFORM)
    from ..platform.default.io.gpio import GPIO
