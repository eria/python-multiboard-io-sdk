from .commons import *
from ...utils.logger import log

try:
    exec("from ..platform." + PLATFORM + ".io.i2c import I2C")
except:
    log.info("I2C interface not implemented for %s" % PLATFORM)
    from ..platform.default.io.i2c import I2C