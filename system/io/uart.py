from .commons import *
from ...utils.logger import log

try:
    exec("from ..platform." + PLATFORM + ".io.uart import UART")
except:
    log.info("UART interface not implemented for %s" % PLATFORM)
    from ..platform.default.io.uart import UART