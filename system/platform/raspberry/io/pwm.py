from ...default.io.pwm import PWM as PWM_IFACE
from .gpio import GPIO
from ....io.commons import *
from ..info import *
import time

# Special thanks to: https://github.com/aboudou/pizypwm/blob/master/quick2wire/pizypwm.py

class PWM(PWM_IFACE):
    MAXCYCLE  = 100.0
    RUNNING = False
    TERMINATING = False
    
    def __init__(self, pin):
        self.PIN = PIN
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.stop()
            
    def start(self, duty=0, freq=2000, polarity=0):
        self.BASETIME  = 1.0 / freq
        self.SLICETIME = self.BASETIME / self.MAXCYCLE
        self.DUTYCYCLE = duty
        
        self.thread = threading.Thread(None, self.run, None, (), {})
        self.thread.start()
    
    def stop(self):
        self.TERMINATING = True
        while self.RUNNING == True:
            time.sleep(0.01)
        
    def run(self):
        with GPIO(self.PIN) as gpio:
            gpio.setup(OUT)
            while self.TERMINATING == False:
                if self.DUTYCYCLE > 0:
                    gpio.output(HIGH)
                    time.sleep(self.DUTYCYCLE * self.SLICETIME)
         
                if self.DUTYCYCLE < self.MAXCYCLE:
                    gpio.output(LOW)
                    time.sleep((self.MAXCYCLE - self.DUTYCYCLE) * self.SLICETIME)

        self.RUNNING = False

            
    def setDutyCycle(self,duty):
        self.DUTYCYCLE = duty
    
    def setFrequency(self,freq):
        self.BASETIME  = 1.0 / freq
        self.SLICETIME = self.BASETIME / self.MAXCYCLE
 

    

