from ....io.commons import *

class PWM(object): 
    def __init__(self, pin):
        raise NotImplementedError( "Should have implemented this" )    
        
    def start(self, duty=0, freq=2000, polarity=0):
        raise NotImplementedError( "Should have implemented this" )
    
    def stop(self):
        raise NotImplementedError( "Should have implemented this" )
            
    def setDutyCycle(self,duty):
        raise NotImplementedError( "Should have implemented this" )
    
    def setFrequency(self,freq):
        raise NotImplementedError( "Should have implemented this" )


