from ....io.commons import *
import os, glob

class GPIO(object):
    PATH = "/sys/class/gpio/"
    
    def __init__(self, pin):
        self.PIN = str(pin)
        
        try:
            f = open(self.PATH + "export", 'w')
            f.write(self.PIN)
            f.close()
        except:
            pass
        
        self.PINPATH = glob.glob(self.PATH + 'gpio' + self.PIN + '*')[0]
        self.VALUEFD = os.open( self.PINPATH + "/value", os.O_RDWR )
        
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.stop()
            
    def setup(self, direction, pull_up_down=PUD_OFF, initial=-1):
        # ToDo: pull, initial (check params)
        with open(self.PINPATH + "/direction", 'w') as f:
            f.write(str(direction)) 

        if direction == IN:
            try: self.write(pull_up_down)
            except: pass

    def write(self, value):
        # Check value
        # CHeck if set as output
        os.lseek(self.VALUEFD, 0, 0)
        os.write(self.VALUEFD, str(value).encode('ascii'))
        
    def read(self):
        os.lseek(self.VALUEFD, 0, 0)
        return int(os.read(self.VALUEFD, 100))
           
    def add_event_callback(self, callback, bouncetime=0):
        raise NotImplementedError( "Should have implemented this" )
        
    def add_event_detect(self, edge, callback=None, bouncetime=0):
        raise NotImplementedError( "Should have implemented this" )
        
    def remove_event_detect(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def event_detected(self):
        raise NotImplementedError( "Should have implemented this" )
        
    def py_wait_for_edge(self, edge):
        raise NotImplementedError( "Should have implemented this" )
    
    def stop(self):
         with open(self.PATH + "unexport", 'w') as f: 
            f.write(self.PIN)


