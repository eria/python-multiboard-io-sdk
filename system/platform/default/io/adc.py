from ....io.commons import *

class ADC(object):
    def __init__(self, pin):
        raise NotImplementedError( "Should have implemented this" )
            
    def read(self):
        raise NotImplementedError( "Should have implemented this" )
    
    
    