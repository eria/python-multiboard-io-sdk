from ....io.commons import *

class SPI(object):
    def __init__(self):
        raise NotImplementedError( "Should have implemented this" )    
    