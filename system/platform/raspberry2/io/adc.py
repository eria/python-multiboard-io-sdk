from ...default.io.adc import ADC as ADC_IFACE
from .gpio import GPIO
from ....io.commons import *
from .....utils.logger import log

##
# Bassed on: http://www.raspberrypi-spy.co.uk/2012/08/reading-analogue-sensors-with-one-gpio-pin/
# With a 10Kohm resistor and a 1uF capacitor t is equal to 1 millisecond.
##

log.warn("ADC is a trick. See: http://www.raspberrypi-spy.co.uk/2012/08/reading-analogue-sensors-with-one-gpio-pin/")

class ADC(ADC_IFACE):
    def __init__(self, pin):
        self.GPIO = GPIO(self.PIN)
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.cleanup()
    
    def cleanup(self):
        self.GPIO.cleanup()
         
    def read(self):
        measurement = 0
        
        # Discharge capacitor
        self.GPIO.setup(OUT)
        self.GPIO.output(LOW)
        time.sleep(0.1)
        
        self.GPIO.setup(IN)
        # Count loops until voltage across
        # capacitor reads high on GPIO
        while (self.GPIO.input() == LOW):
            measurement += 1
        
        return measurement
            

    