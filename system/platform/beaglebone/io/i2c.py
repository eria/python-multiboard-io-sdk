from ...default.io.i2c import I2C as I2C_IFACE

class I2C(I2C_IFACE):
    def __init__(self, bus, extra_open_flags=0):
        if bus < 0 or bus > 1:
            raise
        super().__init__(bus, extra_open_flags)
    

