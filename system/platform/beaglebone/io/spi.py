from ...default.io.spi import SPI as SPI_IFACE

class SPI(I2C_IFACE):
    def __init__(self, bus, extra_open_flags=0):
        if bus < 0 or bus > 1:
            raise
        super().__init__(bus, extra_open_flags)
    

