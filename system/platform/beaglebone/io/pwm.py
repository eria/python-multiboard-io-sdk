from ...default.io.pwm import PWM as PWM_IFACE
from ..info import *
import glob, re

class PWM(PWM_IFACE):

    def __init__(self, pin):
        self.PIN = pin
        self.PINNAME = False

        for index, sublist in enumerate(PIN_TABLE):
            if (sublist[0] == pin or sublist[1] == pin) and sublist[3] != -1:
                self.PIN = PIN_TABLE[index][2]
                self.PINNAME = PIN_TABLE[index][1]
        
        if not self.PINNAME:
            raise
                
        self.PATH = glob.glob('/sys/devices/bone_capemgr*/slots')[0]       
        with open(self.PATH, 'r') as f: 
            content = f.read()
            
        match = re.search( r' *(\d)+:.*am33xx_pwm.*', content, re.M|re.I)
        if not match:
            with open(self.PATH, 'w') as f: 
                f.write("am33xx_pwm")


    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.stop()
            
    def start(self, duty=0, freq=2000, polarity=0):
        with open(self.PATH, 'r') as f: 
            content = f.read()
        match = re.search( r' *(\d)+:.*bone_pwm_' + self.PINNAME + '.*', content, re.M|re.I)
        if not match:
            with open(self.PATH, 'w') as f: 
                f.write("bone_pwm_" + self.PINNAME)
        
        self.setFrequency(freq)
        self.setDutyCycle(duty)
        self.setPolarity(polarity)
    
    def stop(self):
        with open(self.PATH, 'r') as f: 
            content = f.read()
        
        match = re.search( r' *(\d+):.*bone_pwm_'+self.PINNAME+'.*', content, re.M|re.I)
        if match:
            idx = match.group(1)
            with open(self.PATH, 'w') as f: 
                f.write("-" + idx)
     
    def setFrequency(self,freq):
        if freq < 0: raise
        self.FREQ   = freq
        
        path = glob.glob('/sys/devices/ocp.*/pwm_test_' + self.PINNAME + '.*')
        if not path:
            raise

        with open(path[0]+'/period', 'w') as f: 
            f.write(str(int(1000000000/self.FREQ)))
                   
    def setDutyCycle(self,duty):
        if duty < 0 or duty > 100: raise
        self.DUTY = duty
        
        path = glob.glob('/sys/devices/ocp.*/pwm_test_' + self.PINNAME + '.*')
        if not path:
            raise
            
        with open(path[0]+'/duty', 'w') as f: 
            f.write(str(int((1000000000/self.FREQ) * (duty/100))))
    
    
    def setPolarity(self,pol):
        self.POL = pol
        path = glob.glob('/sys/devices/ocp.*/pwm_test_' + self.PINNAME + '.*')
        if not path:
            raise
            
        with open(path[0]+'/polarity', 'w') as f: 
            f.write(str(self.POL))
    

