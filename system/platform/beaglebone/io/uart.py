from ...default.io.uart import UART as UART_IFACE

class UART(UART_IFACE):
    def __init__(self, bus, **kwargs):
        if bus < 1 or bus > 5:
            raise
        
        self.PATH = glob.glob('/sys/devices/bone_capemgr*/slots')[0]       
        with open(self.PATH, 'r') as f: 
            content = f.read()
            
        match = re.search( r' *(\d)+:.*BB-UART'+bus+'.*', content, re.M|re.I)
        if not match:
            with open(self.PATH, 'w') as f: 
                f.write("BB-UART%i" % bus)
        
        kwargs['port'] = "/dev/ttyO%i" % bus
        super().__init__(**kwargs)
    

    

