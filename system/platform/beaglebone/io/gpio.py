from ...default.io.gpio import GPIO as GPIO_IFACE
from ..info import *

class GPIO(GPIO_IFACE):
    def __init__(self, pin):
        found = False
        for index, sublist in enumerate(PIN_TABLE):
            if (sublist[0] == pin or sublist[1] == pin) and sublist[2] != -1:
                found = PIN_TABLE[index][2]

        if not found:
            raise
            
        super().__init__(found)
    
        

    

