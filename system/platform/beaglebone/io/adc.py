from ...default.io.adc import ADC as ADC_IFACE
from ..info import *
import glob, re

class ADC(ADC_IFACE):
    def __init__(self, pin):
        self.PIN = False
        for index, sublist in enumerate(PIN_TABLE):
            if (sublist[0] == pin or sublist[1] == pin) and sublist[4] != -1:
                self.PIN = str(PIN_TABLE[index][4])

        if not self.PIN:
            raise
        
        self.PATH = glob.glob('/sys/devices/bone_capemgr*/slots')[0]       
        with open(self.PATH, 'r') as f: 
            content = f.read()
            
        match = re.search( r' *(\d)+:.*cape-bone-iio.*', content, re.M|re.I)
        if not match:
            with open(self.PATH, 'w') as f: 
                f.write("cape-bone-iio")
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        return
                        
    def read(self):
        path = glob.glob('/sys/devices/ocp.*/helper.*/AIN' + self.PIN)
        if not path:
            return
            
        with open(path[0], 'r') as f: 
            return f.read()
            

    